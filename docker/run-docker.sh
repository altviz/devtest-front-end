#!/bin/bash

workDirectory=$(pwd)/..
echo "Running with workDirectory:$workDirectory"

cd .. && docker run -it --rm \
    --network host \
    -v $workDirectory:/staging \
    --name nodejs-runner altviz/nodejs-runner:latest