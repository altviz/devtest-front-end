# Front End

## Environment Requirements
* node: 11.13.0

Tip: Can use nvm to install this specific version
```
nvm install 11.13.0
```

## Starting application

The application can be run using `react` or vanilla javascript depending on your reference.

Select your directory of choice `react` or `vanilla` from command line.

### Install and run application using webpack dev server
```
npm install
npm run-script devServer
```

## Running in docker (Optional)

To avoid local conflicts with node and/or npm, it may be advisable to run the front end from within a docker image.

### Build and run docker image locally
```
cd docker
./build-docker.sh
./run-docker.sh
```