import React, {Component} from "react";

import Chart from "react-google-charts";

export default class StockChart extends Component {
    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        console.log("componentDidMount");
         // TODO : load data
    }

    prepareData() {
        return [
            ["Period", "Stock"],
            ["p1", 1000],
            ["p2", 1170],
            ["p3", 660],
            ["p4", 1030],
        ];
    }

    render() {
        const chartData = this.prepareData();

        const options = {
            title: "Stock Overview",
            legend: { position: "bottom" }
        };

        return (
            <Chart
                chartType="LineChart"
                width="100%"
                height="400px"
                data={chartData}
                options={options}
            />
        );
    }
}