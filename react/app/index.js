import React from 'react';
import ReactDOM from 'react-dom';
import StockChart from "./StockChart";

class ExampleApp extends React.Component {
    render() {
        return (
            <div>
                <StockChart />
            </div>
        );
    }
}

ReactDOM.render(<ExampleApp />, document.getElementById('id_app'))
