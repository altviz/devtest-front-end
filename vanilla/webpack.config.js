var path = require('path');
var HtmlWebpackPlugin =  require('html-webpack-plugin');

module.exports = {
    entry : './app/index.js',
    output : {
        path : path.resolve(__dirname , 'dist'),
        filename: 'index_bundle.js'
    },
    module : {
        rules : [
            {
                test : /\.(js)$/,
                use:'babel-loader'
            },
            {
                test : /\.css$/, use:['style-loader', 'css-loader']
            },
            {
                test : /\.less$/, use:['style-loader', 'css-loader', 'less-loader' ]
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'svg-url-loader',
                        options: {
                            limit: 10000,
                        },
                    },
                ],
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'img'
                    }
                }]
            }
        ]
    },
    mode:'development',
    plugins : [
        new HtmlWebpackPlugin ({
            template : 'app/index.html',
            hash: true
        })
    ],

    node: {
        fs: "empty",
        'fs-extra': "empty"
    },

    externals: {
        'fs-extra': '{}'
    },

    devServer: {
        contentBase: path.join(__dirname, "dist"),
        port: 9001,
        proxy: {
            '/': {
                target: 'http://localhost:9999',
                secure: false
            }
        }
    }
};