class StockChart {
    prepareData() {
        return [
            ["Period", "Stock"],
            ["p1", 1000],
            ["p2", 1170],
            ["p3", 660],
            ["p4", 1030],
        ];
    }

    render() {
        const chartData = this.prepareData();

        const options = {
            title: "Stock Overview",
            legend: { position: "bottom" }
        };

        const chart = new google.visualization.LineChart(document.getElementById('stock_chart'));
        chart.draw(google.visualization.arrayToDataTable(chartData), options);
    }
}

const stockChart = new StockChart();
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(async () => stockChart.render());

